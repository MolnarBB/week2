#pragma once

#include "summer_school/range/category.hpp"

using summer_school::range::category::generator_tag;

namespace summer_school::range::integers {

	// V. Time to get down to business. We'll implement a range that will generate integers in arithmetic order. For
	// example integers() will generate an "infinite" series of integers starting with zero. integers(1, 2) will generate
	// odd integers starting with 1. integers(0, 5, 20) wil generate 20 multiples of 5 starting with 0. In order to
	// implement a generator you'll need an iterator, a range and function that will return the range.
	//
	// If you are familliar with C++ iterators you probably know there are different categories of iterators depending on
	// what operations they have. Luckily, range iterators require the most simple category and that is the InputIterator.
	// This operator has the following operators:
	// - pre and post incrementation, operator++, goes to the next integer in the series
	// - dereferencing, operator*, returns an integer
	// - equality check, operator== and operator!=
	// Note: In order to use your iterators with std algorithms you need to provide the iterator traits value_type, pointer
	// reference, difference_type and iterator_category.
	class IntegersIterator {
	public:

		using iterator_category = std::input_iterator_tag;

		friend bool operator!=(IntegersIterator a, IntegersIterator b) {
			return a.value_ != b.value_;
		}

		friend bool operator==(IntegersIterator a, IntegersIterator b) {
			return a.value_ == b.value_;
		}


		explicit IntegersIterator(int value) :value_(value) {}

		auto& operator++() {
			++value_;
			return*this;
		}

		auto operator++(int) {
			auto copy = *this;
			++*this;
			return copy;
		}

		auto operator*() const {
			return value_;
		}

	private:
		int value_;
	};

	// VI. The integers generator is a range and like any range it defines two methods, begin() and end(), returning an
	// iterator with the first integer in the sequence and an iterator with the last integer respectively. Don't forget to
	// tag it using your preferred method so you can use it with the pipe operator|.
	class IntegersGenerator {
	public:

		using generator_category = generator_tag;

		IntegersGenerator(int first, int last)
			:first_(first),
			last_(last)
		{}

		IntegersIterator begin()
		{
			return IntegersIterator(first_);
		}

		IntegersIterator end()
		{
			return IntegersIterator(last_);
		}

	private:
		int first_;
		int last_;

	};

	// VII. This function returns the IntegersGenerator. Don't forget to provide default values for the parameters. The
	// function is merely sugar coating so when we chain ranges we don't have to worry about the underlying type of the
	// range. We let the pipe operator| solve the type for us through template parameter deduction.
	// Next is integers.test.cpp.
	inline auto integers(int first, int last) {

		return IntegersGenerator(first, last);
	}

}