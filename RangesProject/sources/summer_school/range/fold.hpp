#pragma once
#include "summer_school/range/category.hpp"

namespace summer_school::range::fold {

// XIV. You have implemented a generator and a view. To make the circle complete you'll now implement a reduction.
// Reductions are the simplest of all. You only need to implement a factory and the fold function. The fold reduction
// takes an initial value and a binary function, and applies the function to the elements of the input range. In other
// words: {1, 2, 3} | fold(0, op) = (((0 op 1) op 2) op 3).
template<typename InitialValue, typename Folder>
class FoldReductionFactory {
public:
	using range_category = summer_school::range::category::reduction_factory_tag;

	explicit FoldReductionFactory(InitialValue init, Folder f)
		:initialValue_(init),
		folder_(f)
	{}

	template<typename InputRange>
	auto create(InputRange&& vec) {
		auto res = initialValue_;
		for (const auto& it : vec)
		{
			folder_(res, it);
		}
		return res;
	}

private:
	InitialValue initialValue_;
	Folder folder_;
};

// XV. Sprinkle a little bit of sugar and off to testing.
template<typename InitialValue, typename Folder>
auto fold(InitialValue initialValue, Folder folder){
	return FoldReductionFactory(initialValue, folder);
};

}
