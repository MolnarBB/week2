#include "catchorg/catch/catch.hpp"
#include "summer_school/range/integers.hpp"

// VIII. Remember that integers() is "infinite". Currently we can't take a slice from this range so use upper bounded
// generators. After this test you'll implement the take view which will allow you to make an infinite range finite.
// goto take.hpp;

using summer_school::range::integers::IntegersGenerator;
using summer_school::range::integers::IntegersIterator;
using summer_school::range::integers::integers;

TEST_CASE("Integers") {

	IntegersGenerator test1(5, 20);
	REQUIRE(*test1.begin() == 5);
	REQUIRE(*test1.end() == 20);



}
