#include "catchorg/catch/catch.hpp"
#include "summer_school/range/pipe.hpp"
#include "summer_school/range/fold.hpp"

using summer_school::range::pipe::operator|;
using summer_school::range::fold::fold;

// XVII. Here you'll test different combinations of containers, generators, views and reductions. After this
// you are done! Congratulations!
TEST_CASE("Pipe") { 

	std::vector<int> vec = { 1, 2, 3 };
	vec | fold(0, [](int a, int b) {return a + b; });
}
