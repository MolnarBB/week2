#include "catchorg/catch/catch.hpp"
#include "summer_school/range/category.hpp"

#include <vector>

using summer_school::range::category::is_pipe_head;
using summer_school::range::category::is_range_factory;
using summer_school::range::category::view_tag;
using summer_school::range::category::view_factory_tag;
using summer_school::range::category::reduction_tag;
using summer_school::range::category::container_tag;
using summer_school::range::category::generator_tag;
using summer_school::range::category::reduction_factory_tag;

struct SomeView {
	using range_category = view_tag;
};

struct ViewFactory {
	using range_category = view_factory_tag;
};

struct Inherited : public reduction_factory_tag
{

};

// III. You should test if is_pipe_head and is_range_factory behave well. You can use mocks. These are dummy classes
// that only provide the functionality needed to run your tests. Also don't forget to test vectors.
//
// Quick introduction to testing
// Use REQUIRE statements to check if a condition is true. We provided a couple of examples. Your tests will run
// automatically when you run the application and the results will be displayed in the console. When you are
// satisfied with your tests jump to the pipe.hpp file.


TEST_CASE("Category") {
    REQUIRE(is_pipe_head<std::vector<int>>);

    REQUIRE(!is_range_factory<std::vector<int>>);

	REQUIRE(is_pipe_head<SomeView>);

	REQUIRE(is_range_factory<Inherited>);
    /* YOUR CODE HERE */
}
